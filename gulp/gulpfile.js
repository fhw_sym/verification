// 変換するscssの場所
const scssSrc = "../css/*.scss";
// 変換するcssの場所
const cssSrc = "../css/*.css";
// cssを保存するディレクトリ
const cssSaveDir = "../css/";
// babelを実行するディレクトリ
const jsSrc = "**/*.js";
// jsを保存するディレクトリ
const jsSaveDir = "../js/babel";

//　モジュールの読み込み
const gulp = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const babel = require("gulp-babel");

// cssをscssに変換
gulp.task("sass", () => {
  return gulp
    .src(scssSrc)
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(gulp.dest(cssSaveDir));
});

// watchでscssを監視
gulp.task("watch", () => {
  return gulp.watch(scssSrc, gulp.task("sass"));
});

// autoprefixer
gulp.task("autoprefixer", () => {
  return gulp.src(cssSrc).pipe(autoprefixer()).pipe(gulp.dest(cssSaveDir));
});

// babel
gulp.task("babel", () => {
  return gulp.src(jsSrc).pipe(babel()).pipe(gulp.dest(jsSaveDir));
});

// defaultの設置
gulp.task("default", gulp.task("sass"));
